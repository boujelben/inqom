const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({ headless: false });// ouvrir un navigateur Chromium en mode visible
  const context = await browser.newContext(); // créer un nouvel onglet
  const page = await context.newPage(); // ouvrir une nouvelle page web


   // Set a custom "Cookie" header with an empty value
   await page.setExtraHTTPHeaders({
    'Cookie': ''
  });


  // Navigate to the website
  await page.goto('http://www.welcometothejungle.com/fr/me/settings/account');

  await Promise.all([
    page.waitForNavigation(), // attendre que la page de connexion se charge
    page.click('#login') // cliquer sur le bouton "Se connecter"
  ]);



  // Fill in the email and password fields and submit the form
  await page.fill('#email_login', 'inqom.qaautomationapplicant@gmail.com');
  await page.fill('#password', 'o5N,d5ZR@R7^');
  await Promise.all([
    page.waitForNavigation(),
    page.click('button[type="submit"]'),
  ]);

  // Upload a random photo
  const input = await page.$('input[type=file]');
  await input.setInputFiles('path/to/random/photo');

  // Click the "OK" button
  await Promise.all([
    page.waitForNavigation(),
    page.click('button[type="submit"]'),
  ]);


  // Assert that the profile photo was uploaded successfully
  const profilePhoto = await page.$('.profile-picture img');
  const photoSrc = await profilePhoto.getAttribute('src');
  // Make sure the photo source is not the default one
  expect(photoSrc).not.toContain('default_profile_picture');
  
  await browser.close();
})();